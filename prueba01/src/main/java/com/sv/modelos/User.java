package com.sv.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user" , catalog="usuarios", schema="")
public class User {

	@Id
	@Column
	private String mail;
	@Column 
	private String estilo_musica;
	
	
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getEstilo_musica() {
		return estilo_musica;
	}
	public void setEstilo_musica(String estilo_musica) {
		this.estilo_musica = estilo_musica;
	}
	
	
	
	
	
}
